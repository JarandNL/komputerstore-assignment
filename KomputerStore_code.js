//Elements of the bank section:
const balanceElement = document.getElementById("balance");
const outstandingLoanElement = document.getElementById("outstanding-loan");
const getLoanElement = document.getElementById("getLoanButton");

//Elements of the work section:
const currentMoneyElement = document.getElementById("earnedMoney");
const depositMoneyElement = document.getElementById("depositButton");
const earnMoneyElement = document.getElementById("earnButton");
const payLoanElement = document.getElementById("payLoanButton");

//Elements of the laptops section:
const dropLaptopElement = document.getElementById("dropDownLaptopNames");
const laptopFeatureElement = document.getElementById("laptopFeature");

//Other constants
const loanPercentage = 0.1; //This equals 10% which will be withdrawn automatically from earned money to current loan if existing.
const maxLoan = 2; //This equals 2x balance. You can maximum loan twice your balance.

/// Converting num to norwegian format (NOK/kr)
const numToNok = (num) => {
  return new Intl.NumberFormat("no-NO", {
    style: "currency",
    currency: "NOK",
  }).format(num);
};

//Global variables
let balance = 0;
let outstandingLoan = 0;
let currentEarnedMoney = 0;

//Assigning the correct currency to balance and earned money.
balanceElement.innerText = `Balance: ${numToNok(balance)}`;
currentMoneyElement.innerText = `Pay: ${numToNok(balance)}`;

//Bank - Functionality:

//Function to handle loan requests
function handleLoanRequest() {
  if (outstandingLoan > 0) {
    alert(
      "You can not have multiple loans, pay your loan before requesting a new one!"
    );
  } else {
    let requestedLoan = loanAmount();
    if (requestedLoan) {
      let validLoan = checkBalance(requestedLoan);
      if (validLoan) {
        outstandingLoan = requestedLoan;
        balance += Number(outstandingLoan);
        balanceElement.innerText = `Balance: ${numToNok(balance)}`;
        outstandingLoanElement.innerText = `Outstanding loan: ${numToNok(
          outstandingLoan
        )}`;
        //If loan: Show outstanding loan & pay loan button
        outstandingLoanElement.hidden = false;
        payLoanElement.hidden = false;
      }
    }
  }
}
//Event listener for get loan button
getLoanElement.addEventListener("click", handleLoanRequest);

//function that asks for input loan, and check if its not a number
function loanAmount() {
  try {
    const inputLoan = window.prompt("How much would you like to loan?");
    if (isNaN(inputLoan)) {
      alert("Invalid input. Enter a number:");
      return false;
    }
    return inputLoan;
  } catch (error) {
    console.log(error);
    alert("Invalid input. Enter a number:");
    return false;
  }
}
//Function to check if balance is high enough to get a specific loan
function checkBalance(requestedLoan) {
  if (requestedLoan > balance * maxLoan) {
    alert(
      "You don't have enough money for this loan. You can loan maximum two times your balance."
    );
    return false;
  } else if (requestedLoan <= 0) {
    alert("You can not loan this amount of money. Try again");
  } else {
    return true;
  }
}

// Work - Functionality

//Event listener for work button - increase pay with 100 on click.
earnMoneyElement.addEventListener("click", () => {
  currentEarnedMoney += 100;
  currentMoneyElement.innerText = `Pay: ${numToNok(currentEarnedMoney)}`;
});

//Handling action on bank deposit button
function handleBankDeposit() {
  if (outstandingLoan === 0) {
    balance += currentEarnedMoney;
    console.log(balance);
    balanceElement.innerText = `Balance: ${numToNok(balance)}`;
  }
  //Auto pays 10% to loan if loan exist
  else if (outstandingLoan >= currentEarnedMoney * loanPercentage) {
    outstandingLoan -= currentEarnedMoney * loanPercentage;
    outstandingLoanElement.innerText = `Outstanding Loan: ${numToNok(
      outstandingLoan
    )}`;
    balance += currentEarnedMoney * (1 - loanPercentage); // Adding currentEarned * (1 - 0.1) to automatically pay 10% to loan
    balanceElement.innerText = `Balance: ${numToNok(balance)}`;
  }
  //If loan is less then 10% of the, more then 90% goes to balance
  else if (0 < outstandingLoan < currentEarnedMoney * loanPercentage) {
    balance += currentEarnedMoney - outstandingLoan;
    balanceElement.innerText = `Balance: ${numToNok(balance)}`;
    outstandingLoan = 0;
    outstandingLoanElement.innerText = `Outstanding Loan: ${numToNok(
      outstandingLoan
    )}`;
  }
  //Resetting earned money after depositing
  currentEarnedMoney = 0;
  currentMoneyElement.innerText = `Pay: ${numToNok(currentEarnedMoney)}`;
  //Setting outstanding loan back to hidden if there is no current loan.
  if (outstandingLoan === 0) {
    outstandingLoanElement.hidden = true;
    payLoanElement.hidden = true;
  }
}
//Event listener for bank button
depositMoneyElement.addEventListener("click", handleBankDeposit);

//Handling action on clicking pay loan button
function handlePayLoan() {
  if (outstandingLoan >= currentEarnedMoney) {
    outstandingLoan -= currentEarnedMoney;
  }
  //Remaining money after paid loan goes to balance
  else if (outstandingLoan < currentEarnedMoney) {
    balance += currentEarnedMoney - outstandingLoan;
    balanceElement.innerText = `Balance: ${numToNok(balance)}`;
    outstandingLoan = 0;
  }
  outstandingLoanElement.innerText = `Outstanding Loan: ${numToNok(
    outstandingLoan
  )}`;
  currentEarnedMoney = 0;
  currentMoneyElement.innerText = `Pay: ${numToNok(currentEarnedMoney)}`;
  //Setting outstanding loan back to hidden if there is no current loan.
  if (outstandingLoan === 0) {
    outstandingLoanElement.hidden = true;
    payLoanElement.hidden = true;
  }
}
//Event listener for the pay loan button
payLoanButton.addEventListener("click", handlePayLoan);

// Laptop - functionality

let laptopsFetch = [];
let laptopTitle = "";
let laptopPrice = 0;

// Fetching data for the laptops
fetch("https://hickory-quilled-actress.glitch.me/computers")
  .then((response) => response.json())
  .then((data) => (laptops = data))
  .then((data) => (laptopsFetch = data))
  .then((laptops) => addLaptopsToDropDownList(laptops))
  .catch((error) => console.error("Error: ", error.message));

//adding laptops into the <select> drop down list
const addLaptopsToDropDownList = (laptops) => {
  laptops.forEach((laptop) => laptopToList(laptop));
};
//Converting a laptop to an option and adds it to the list.
const laptopToList = (laptop) => {
  const optionElement = document.createElement("option");
  optionElement.text = laptop.title;
  dropLaptopElement.appendChild(optionElement);
};

//Elements of the laptop info
const chosenLaptopImage = document.getElementById("chosenImage");
const chosenLaptopTitle = document.getElementById("chosenLaptopTitle");
const chosenLaptopDescription = document.getElementById(
  "chosenLaptopDescription"
);
const chosenLaptopPrice = document.getElementById("chosenLaptopPrice");
const buyChosenLaptop = document.getElementById("buyButton");

//Handling laptop changes (image, title, price etc.)
const handleLaptopChange = (e) => {
  let newList;
  laptopFeatureElement.innerText = "";
  const selectedLaptop = laptopsFetch[e.target.selectedIndex - 1];
  for (const str in selectedLaptop.specs) {
    newList = document.createElement("newList");
    newList.appendChild(document.createTextNode(selectedLaptop.specs[str]));
    laptopFeatureElement.appendChild(newList);
  }
  chosenLaptopTitle.innerText = selectedLaptop.title;
  chosenLaptopDescription.innerText = selectedLaptop.description;
  chosenLaptopPrice.innerText = numToNok(selectedLaptop.price);
  laptopTitle = selectedLaptop.title;
  laptopPrice = selectedLaptop.price;
  chosenLaptopImage.setAttribute(
    "src",
    "https://hickory-quilled-actress.glitch.me/" + selectedLaptop.image
  );
};

//Event listener for handling chosen laptop changes
dropDownLaptopNames.addEventListener("change", handleLaptopChange);

//Function to handle buy section. I made it able to use its outstanding loan to purchase more expensive computers
function handleBuyLaptop() {
  //Using loan to buy
  if (balance < laptopPrice) {
    alert(
      `You can not afford ${laptopTitle}, this laptop will cost you ${laptopPrice}.`
    );
  } else {
    //Updating balance after purchase
    balance = balance - laptopPrice;
    balanceElement.innerText = `Balance: ${numToNok(balance)}`;
    alert(`You just purchased ${laptopTitle}, enjoy your laptop!`);
  }
}
buyChosenLaptop.addEventListener("click", handleBuyLaptop);
