### Komputer Store - Create a dynamic webpage using JS

## Description
In this assignment I built a dynamic webpage using vanilla JavaScript. This was done in Visual Studio Code. I wrote html, css and plain javascript.Css was written for customization purposes. There are three main sections in this assignment; Bank, Work & Laptop.

## Komputer Store - Requirements
Bank - In this section you have a balance and a get loan button. If this button is pressed and the requirements are met, Outstanding loan will be displayed in the bank section. Paying back your loan will remove this.

Work - In this section you have a work balance, and two buttons; work and bank. Here you can press work to earn 100kr. This amount will add to you work balance on each click. If you press the Bank button, you will deposit your work balance to your bank balance. If you have a loan active, you get an additional button displayed called pay loan. Pressing this button will add all your work money directly to loan payment, instead of 10% which will be deposited if you press the bank button while having a loan.

Laptops - In this section you'll see a dropdown list with all the available laptops. When selecting a laptop, you'll be displayed its features(specs), title, description, image & price. There will also be a buy now button available. If the requirements are met, you can buy the selected laptop. This will of course affect your bank balance.

## Technologies
* HTML
* CSS
* Javascript

## Hosting
[![Netlify Status](https://api.netlify.com/api/v1/badges/153a5cec-0949-4956-bb62-60244aa4b612/deploy-status)](https://app.netlify.com/sites/jarandkomputerstore/deploys) or https://jarandkomputerstore.netlify.app/


## Contributors
This assignment is created by Jarand
